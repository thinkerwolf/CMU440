package main

import (
	"testing"

	"github.com/cmu440/p0partA"
	"github.com/cmu440/p0partA/kvstore"
)

func TestKvServer(t *testing.T) {
	store, _ := kvstore.CreateWithBackdoor()
	kvs := p0partA.New(store)
	err := kvs.Start(8080)
	if err != nil {
		t.Log(err)
		t.Errorf("Start server fail : %s", err)
	}
}

/*func TestByte(t *testing.T) {
	bs := []byte("Put:key_66:value_66_0")
	t.Log("test: ", ('\n' == 0))
	t.Log("test append", string(append(bs, '\n')))
	rwmutex := new(sync.RWMutex)
	rwmutex.RLock()
	defer rwmutex.RUnlock()
}*/
