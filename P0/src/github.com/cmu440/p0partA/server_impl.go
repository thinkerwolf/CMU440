// Implementation of a KeyValueServer. Students should write their code in this file.

package p0partA

import (
	"bufio"
	"fmt"
	"net"
	"strconv"
	"strings"

	"github.com/cmu440/p0partA/kvstore"
)

const (
	largeSize = 4096
	size      = 100
)

type readEvent struct {
	conn net.Conn
	bs   []byte
	rw   *bufio.ReadWriter
}

type keyValueServer struct {
	// TODO: implement this!
	store    kvstore.KVStore
	droped   int
	alived   int
	listener net.Listener
	ch       chan *readEvent
}

// New creates and returns (but does not start) a new KeyValueServer.
func New(store kvstore.KVStore) KeyValueServer {
	// TODO: implement this!
	kvserver := keyValueServer{
		store: store,
		ch:    make(chan *readEvent),
	}
	return &kvserver
}

func (kvs *keyValueServer) Start(port int) error {
	// TODO: implement this!
	address := ":" + strconv.Itoa(port)
	_, err := net.ResolveTCPAddr("tcp", address)
	if err != nil {
		//fmt.Errorf("ResolveTCPAddr fail: %s", err)
		return err
	}
	fmt.Println("Listen on address : ", address)
	listener, err := net.Listen("tcp", address)
	if err != nil {
		// fmt.Errorf("Listen fail: %s", err)
		return err
	}
	kvs.listener = listener
	go handleMsg(kvs)
	go accept(kvs)
	return nil
}

func (kvs *keyValueServer) Close() int {
	// TODO: implement this!
	if kvs.listener != nil {
		kvs.listener.Close()
	}
	return -1
}

func (kvs *keyValueServer) CountActive() int {
	// TODO: implement this!
	return kvs.alived
}

func (kvs *keyValueServer) CountDropped() int {
	// TODO: implement this!
	return kvs.droped
}

// TODO: add additional methods/functions below!

func accept(kvs *keyValueServer) {
	for {
		conn, err := kvs.listener.Accept()
		if err != nil {
			break
		}
		go handler(kvs, conn, err)
	}
}

func handler(kvs *keyValueServer, conn net.Conn, err error) {
	// need synchronization
	kvs.alived++
	key := conn.RemoteAddr().String()
	fmt.Println("connection is connected from ...", key)
	rw := bufio.NewReadWriter(bufio.NewReader(conn), bufio.NewWriter(conn))

	for {
		bs, err := rw.ReadBytes('\n')
		if err != nil {
			kvs.alived--
			kvs.droped++
			break
		}
		kvs.ch <- &readEvent{conn: conn, bs: bs, rw: rw}
	}
}

func handleMsg(kvs *keyValueServer) {
	for {
		event := <-kvs.ch
		rw := event.rw
		bs := event.bs
		if len := len(bs); len > largeSize {
			bs = bs[0 : len-largeSize]
		} else {
			bs = bs[0 : len-1]
		}
		m := string(bs)
		msg := strings.Split(m, ":")
		switch msg[0] {
		case "Put":
			fmt.Println("Put key:", msg[1], " value:", msg[2])
			kvs.store.Put(msg[1], []byte(msg[2]))
		case "Delete":
			fmt.Println("Delete key:", msg[1], " exists:", (kvs.store.Get(msg[1]) != nil))
			kvs.store.Clear(msg[1])
		case "Get":
			fmt.Println("Get key:", msg[1], " exists:", (kvs.store.Get(msg[1]) != nil))
			key := msg[1]
			for _, v := range kvs.store.Get(key) {
				_, err := rw.Write([]byte(fmt.Sprintf("%s:%s\n", key, string(v))))
				if err != nil {
					fmt.Println(err)
				} else {
					rw.Flush()
				}
			}
		default:

		}
	}
}
