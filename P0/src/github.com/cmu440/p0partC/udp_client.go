package p0partC

import (
	"net"
)

func StartClient(port int) (*net.UDPConn, error) {
	ip := net.ParseIP("127.0.0.1")
	laddr := &net.UDPAddr{IP: net.IPv4zero, Port: 0}
	raddr := &net.UDPAddr{IP: ip, Port: port}
	conn, err := net.DialUDP("udp", laddr, raddr)
	if err != nil {
		return nil, err
	}
	return conn, nil
}
