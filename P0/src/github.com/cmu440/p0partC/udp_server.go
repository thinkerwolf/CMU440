package p0partC

import (
	"fmt"
	"net"
)

type readEvent struct {
	bs   []byte
	addr *net.UDPAddr
}

func StartServer(port int) (*net.UDPConn, error) {
	laddr := net.UDPAddr{
		IP:   net.ParseIP("0.0.0.0"),
		Port: port,
	}
	conn, err := net.ListenUDP("udp", &laddr)
	if err != nil {
		return nil, err
	}
	readChan := make(chan *readEvent)
	go process(conn, readChan)
	go func() {
		for {
			select {
			case cmd := <-readChan:
				fmt.Println("Receive from ", cmd.addr, " msg:", string(cmd.bs))
			}
		}
	}()
	return conn, nil
}

func process(conn *net.UDPConn, readChan chan<- *readEvent) {
	buf := make([]byte, 4096)
	for {
		n, addr, err := conn.ReadFromUDP(buf)
		if err != nil {
			break
		}
		data := buf[0:n]
		readChan <- &readEvent{
			bs:   data,
			addr: addr,
		}
	}
}
