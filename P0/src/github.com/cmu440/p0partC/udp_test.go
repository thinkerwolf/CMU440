package p0partC

import (
	"fmt"
	"net"
	"strconv"
	"testing"
	"time"
)

var largeByteSlice = make([]byte, 2048)

var smallByteSlice = make([]byte, 1)

type networkEvent struct {
	cli      *net.UDPConn
	writeMsg string
}

func TestBasic(t *testing.T) {
	port := 9081
	clientNum := 2
	msgNums := 10
	writeChan := make(chan *networkEvent)
	StartServer(port)

	time.Sleep(time.Duration(100) * time.Millisecond)
	clients := make([]*net.UDPConn, clientNum)
	for i := 0; i < clientNum; i++ {
		c, _ := StartClient(port)
		clients[i] = c
	}
	time.Sleep(time.Duration(1000) * time.Millisecond)

	timeoutChan := time.After(time.Duration(10000) * time.Millisecond)

	startWriting(clients, writeChan, msgNums)
	for {
		isTimeout := false
		select {
		case cmd := <-writeChan:
			cli := cmd.cli
			bs := []byte(fmt.Sprintf("value_%s%s\n", cmd.writeMsg, largeByteSlice))
			cli.Write(bs)
		case <-timeoutChan:
			isTimeout = true
		}
		if isTimeout {
			break
		}
	}
	t.Errorf("Test over")
}

func startWriting(clients []*net.UDPConn, writeChan chan<- *networkEvent, numMsgs int) {
	for _, cli := range clients {
		client := cli
		go func() {
			for c := 1; c <= numMsgs; c++ {
				writeChan <- &networkEvent{
					cli:      client,
					writeMsg: strconv.Itoa(c),
				}
				if c%10 == 0 {
					time.Sleep(time.Duration(100) * time.Millisecond)
				}
			}
		}()
	}
}
