package raft

import (
	"fmt"
	"math/rand"

	"github.com/cmu440/rpc"
)

var (
	defaultElectionTimeout   = 200
	defaultHeartbeatInterval = 100
)

type Peer struct {
	serverID  int
	voteChan  chan *VoteEvent
	clientEnd *rpc.ClientEnd
}

type VoteEvent struct {
	args  *RequestVoteArgs
	reply *RequestVoteReply
	suc   bool
}

// GetElectionTimeout 获取下一次选举的超时时间
func GetElectionTimeout(t int) int {
	return rand.Intn(t) + t
}

func (args *RequestVoteArgs) String() string {
	return fmt.Sprintf("[%d %d %d %d]", args.Term, args.CandidateID, args.LastLogTerm, args.LastLogIndex)
}

func (reply *RequestVoteReply) String() string {
	return fmt.Sprintf("[%d %t]", reply.Term, reply.VoteGranted)
}
