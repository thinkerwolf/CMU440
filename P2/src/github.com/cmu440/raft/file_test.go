package raft

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"testing"
)

func TestReadFile(t *testing.T) {
	file, err := os.OpenFile("test.txt", os.O_CREATE|os.O_RDWR|os.O_APPEND, os.ModePerm)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()
	rw := bufio.NewReadWriter(bufio.NewReader(file), bufio.NewWriter(file))
	rw.WriteString("jj\n")
	rw.WriteString("vbv\n")
	rw.Flush()
	for {
		inputString, readerError := rw.ReadString('\n')
		fmt.Printf("The input was: %s", inputString)
		if readerError == io.EOF {
			return
		}
	}
}

func TestWindowsFile(t *testing.T) {
	err := os.MkdirAll("C:/raftlogs/", os.ModePerm)
	if err != nil {
		t.Errorf("MkdirAll error %s", err)
		return
	}
	file, err := os.OpenFile("C:/raftlogs/test.txt", os.O_CREATE|os.O_RDWR|os.O_APPEND, os.ModePerm)
	if err != nil {
		t.Errorf("OpenFile error %s", err)
		return
	}
	defer file.Close()
	rw := bufio.NewReadWriter(bufio.NewReader(file), bufio.NewWriter(file))
	rw.WriteString("jj\n")
	rw.WriteString("vbv\n")
	rw.Flush()
	for {
		inputString, readerError := rw.ReadString('\n')
		fmt.Printf("The input was: %s", inputString)
		if readerError == io.EOF {
			return
		}
	}
}

func TestAppendLog(t *testing.T) {
	appendLog := NewPersistentLog("./", "append_log.txt")
	appendLog.Init()
	appendLog.AppendLog(2, &ApplyCommand{Index: 1, Command: "7008"})
	t.Logf("AppendLog content ====== \n%s\n", appendLog.String())

	t.Errorf("Test success")
}
