//
// raft.go
// =======
// Write your code in this file
// We will use the original version of all other
// files for testing
//

package raft

//
// API
// ===
// This is an outline of the API that your raft implementation should
// expose.
//
// rf = NewPeer(...)
//   Create a new Raft server.
//
// rf.PutCommand(command interface{}) (index, term, isleader)
//   PutCommand agreement on a new log entry
//
// rf.GetState() (me, term, isLeader)
//   Ask a Raft peer for "me" (see line 58), its current term, and whether it thinks it
//   is a leader
//
// ApplyCommand
//   Each time a new entry is committed to the log, each Raft peer
//   should send an ApplyCommand to the service (e.g. tester) on the
//   same server, via the applyCh channel passed to NewPeer()
//

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sync"
	"time"

	"github.com/cmu440/rpc"
)

// Set to false to disable debug logs completely
// Make sure to set kEnableDebugLogs to false before submitting
const kEnableDebugLogs = true

// Set to true to log to stdout instead of file
const kLogToStdout = true

// Change this to output logs to a different directory
const kLogOutputDir = "C:/raftlogs/"

// Role 服务器角色
type Role int32

const (
	// Leader leader
	Leader Role = iota
	// Follower follower
	Follower
	// Candidater candidater
	Candidater
)

//
// ApplyCommand
// ========
//
// As each Raft peer becomes aware that successive log entries are
// committed, the peer should send an ApplyCommand to the service (or
// tester) on the same server, via the applyCh passed to NewPeer()
//
type ApplyCommand struct {
	Index   int
	Command interface{}
}

//
// Raft struct
// ===========
//
// A Go object implementing a single Raft peer
//
type Raft struct {
	mux   sync.Mutex       // Lock to protect shared access to this peer's state
	peers []*rpc.ClientEnd // RPC end points of all peers
	me    int              // this peer's index into peers[]
	// You are expected to create reasonably clear log files before asking a
	// debugging question on Piazza or OH. Use of this logger is optional, and
	// you are free to remove it completely.
	logger *log.Logger // We provide you with a separate logger per peer.

	// Your data here (3A, 3B).
	// Look at the Raft paper's Figure 2 for a description of what
	// state a Raft peer should maintain
	role      Role // 当前服务器角色
	wrapPeers []*Peer

	log *PersistentLog

	currentTerm int // The latest term
	votedFor    int // 当前term投票的候选人id
	commitIndex int // 已提交的log最大索引
	lastApplied int //  提交到状态机的log最大索引

	// only for leaders
	nextIndex  map[int]int // 发送到所有Followers的下一个index。从大到小
	matchIndex map[int]int // 复制到其他服务器的最大log的index。从小到大

	//heartbeat       int32
	voteResultChan    chan *VoteEvent
	electionTimer     *time.Timer
	electionTimeout   int
	heartbeatInverval int
}

//
// GetState()
// ==========
//
// Return "me", current term and whether this peer
// believes it is the leader
//
func (rf *Raft) GetState() (int, int, bool) {
	var me int
	var term int
	var isleader bool
	// Your code here (3A)
	rf.mux.Lock()
	me = rf.me
	term = rf.currentTerm
	isleader = rf.role == Leader
	rf.mux.Unlock()
	return me, term, isleader
}

//
// RequestVoteArgs
// ===============
//
// Example RequestVote RPC arguments structure
//
// Please note
// ===========
// Field names must start with capital letters!
//
type RequestVoteArgs struct {
	// Your data here (3A, 3B)
	Term         int // 候选term
	CandidateID  int // 候选ID
	LastLogTerm  int // 上一次log termD
	LastLogIndex int // 上一次log 索引
}

//
// RequestVoteReply
// ================
//
// Example RequestVote RPC reply structure.
//
// Please note
// ===========
// Field names must start with capital letters!
//
//
type RequestVoteReply struct {
	// Your data here (3A)
	Term        int  // 当前的term
	VoteGranted bool // 候选人是否获得投票
}

//
// RequestVote
// ===========
//
// Example RequestVote RPC handler
//
func (rf *Raft) RequestVote(args *RequestVoteArgs, reply *RequestVoteReply) {
	// Your code here (3A, 3B)
	if args == nil {
		rf.logger.Printf("RequestVoteArgs is nil\n")
	}

	vote := false
	if rf.log.LastTerm <= args.LastLogTerm {
		b1 := rf.votedFor < 0 || rf.votedFor == args.CandidateID
		b2 := rf.log.LastIndex <= args.LastLogIndex
		if b1 && b2 {
			rf.votedFor = args.CandidateID
			vote = true
		}
	}
	reply.VoteGranted = vote
	if rf.currentTerm > args.Term {
		reply.Term = rf.currentTerm
	} else {
		reply.Term = args.Term
	}
}

// AppendEntriesArgs AppendEntries arguments
type AppendEntriesArgs struct {
	LeaderID     int
	Term         int
	CommitIndex  int
	PrevLogIndex int
	PrevLogTerm  int
	Entries      []byte
}

// AppendEntriesReply AppendEntries replay
type AppendEntriesReply struct {
	Term    int
	Success bool
}

func (rf *Raft) sendAppendEntries(server int, args *AppendEntriesArgs, replay *AppendEntriesReply) bool {
	return rf.peers[server].Call("Raft.AppendEntries", args, replay)
}

// AppendEntries 追加log文件或者heartbeat
func (rf *Raft) AppendEntries(args *AppendEntriesArgs, replay *AppendEntriesReply) {
	if args.Entries == nil {
		// heartbeat
		rf.logger.Printf("Reft %d receive heartbeat", rf.me)
		rf.mux.Lock()
		rf.role = Follower
		replay.Success = true
		replay.Term = rf.currentTerm
		rf.electionTimer.Reset(time.Duration(GetElectionTimeout(rf.electionTimeout)) * time.Millisecond)
		rf.mux.Unlock()
	} else {
		// append log
		rf.logger.Printf("Reft %d receive append", rf.me)
		if args.Term < rf.currentTerm {
			replay.Success = false
		} else if !rf.log.CheckPre(args.PrevLogTerm, args.PrevLogIndex) {
			replay.Success = false
		} else {

		}
		if rf.currentTerm > args.Term {
			replay.Term = rf.currentTerm
		} else {
			replay.Term = args.Term
		}
	}
}

//
// sendRequestVote
// ===============
//
// Example code to send a RequestVote RPC to a server
//
// server int -- index of the target server in
// rf.peers[]
//
// args *RequestVoteArgs -- RPC arguments in args
//
// reply *RequestVoteReply -- RPC reply
//
// The types of args and reply passed to Call() must be
// the same as the types of the arguments declared in the
// handler function (including whether they are pointers)
//
// The rpc package simulates a lossy network, in which servers
// may be unreachable, and in which requests and replies may be lost
//
// Call() sends a request and waits for a reply
//
// If a reply arrives within a timeout interval, Call() returns true;
// otherwise Call() returns false
//
// Thus Call() may not return for a while
//
// A false return can be caused by a dead server, a live server that
// can't be reached, a lost request, or a lost reply
//
// Call() is guaranteed to return (perhaps after a delay)
// *except* if the handler function on the server side does not return
//
// Thus there
// is no need to implement your own timeouts around Call()
//
// Please look at the comments and documentation in ../rpc/rpc.go
// for more details
//
// If you are having trouble getting RPC to work, check that you have
// capitalized all field names in the struct passed over RPC, and
// that the caller passes the address of the reply struct with "&",
// not the struct itself
//
func (rf *Raft) sendRequestVote(server int, args *RequestVoteArgs, reply *RequestVoteReply) bool {
	ok := rf.peers[server].Call("Raft.RequestVote", args, reply)
	return ok
}

//
// PutCommand
// =====
//
// The service using Raft (e.g. a k/v server) wants to start
// agreement on the next command to be appended to Raft's log
//
// If this server is not the leader, return false
//
// Otherwise start the agreement and return immediately
//
// There is no guarantee that this command will ever be committed to
// the Raft log, since the leader may fail or lose an election
//
// The first return value is the index that the command will appear at
// if it is ever committed
//
// The second return value is the current term
//
// The third return value is true if this server believes it is
// the leader
//
func (rf *Raft) PutCommand(command interface{}) (int, int, bool) {
	index := -1
	term := -1
	isLeader := true

	// Your code here (3B)

	return index, term, isLeader
}

//
// Stop
// ====
//
// The tester calls Stop() when a Raft instance will not
// be needed again
//
// You are not required to do anything
// in Stop(), but it might be convenient to (for example)
// turn off debug output from this instance
//
func (rf *Raft) Stop() {
	// Your code here, if desired
}

//
// NewPeer
// ====
//
// The service or tester wants to create a Raft server
//
// The port numbers of all the Raft servers (including this one)
// are in peers[]
//
// This server's port is peers[me]
//
// All the servers' peers[] arrays have the same order
//
// applyCh
// =======
//
// applyCh is a channel on which the tester or service expects
// Raft to send ApplyCommand messages
//
// NewPeer() must return quickly, so it should start Goroutines
// for any long-running work
//
func NewPeer(peers []*rpc.ClientEnd, me int, applyCh chan ApplyCommand) *Raft {
	rf := &Raft{}
	rf.peers = peers
	rf.me = me

	if kEnableDebugLogs {
		peerName := peers[me].String()
		logPrefix := fmt.Sprintf("%s ", peerName)
		if kLogToStdout {
			rf.logger = log.New(os.Stdout, peerName, log.Lmicroseconds|log.Lshortfile)
		} else {
			err := os.MkdirAll(kLogOutputDir, os.ModePerm)
			if err != nil {
				panic(err.Error())
			}
			logOutputFile, err := os.OpenFile(fmt.Sprintf("%s/%s.txt", kLogOutputDir, logPrefix), os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
			if err != nil {
				panic(err.Error())
			}
			rf.logger = log.New(logOutputFile, logPrefix, log.Lmicroseconds|log.Lshortfile)
		}
		rf.logger.Println("logger initialized")
	} else {
		rf.logger = log.New(ioutil.Discard, "", 0)
	}
	// Your initialization code here (3A, 3B)
	rf.electionTimeout = defaultElectionTimeout
	rf.heartbeatInverval = defaultHeartbeatInterval
	rf.votedFor = -1
	rf.role = Follower
	rf.log = NewPersistentLog(kLogOutputDir, fmt.Sprintf("raft_log_%d.rft", me))
	err := rf.log.Init()
	if err != nil {
		rf.logger.Printf("Raft log init error %s", err)
		return nil
	}

	rf.currentTerm = rf.log.LastTerm
	rf.commitIndex = rf.log.LastIndex
	rf.voteResultChan = make(chan *VoteEvent)
	rf.wrapPeers = make([]*Peer, len(rf.peers))
	for i, v := range rf.peers {
		peer := &Peer{serverID: i, voteChan: make(chan *VoteEvent), clientEnd: v}
		rf.wrapPeers[i] = peer
		if i == me {
			go meVoteLoop(rf)
		} else {
			go peerVoteLoop(rf, peer)
		}
	}
	rf.electionTimer = time.NewTimer(time.Duration(GetElectionTimeout(rf.electionTimeout)) * time.Millisecond)
	go electionLoop(rf)
	rf.logger.Printf("Raft server %d start", me)
	return rf
}

func electionLoop(rf *Raft) {
	for !checkLoopStop(rf) {
		<-rf.electionTimer.C
		if rf.role != Leader {
			rf.mux.Lock()
			rf.currentTerm++
			rf.role = Candidater
			rf.votedFor = -1
			if len(rf.peers) < 2 { //单机
				rf.role = Leader
			} else {
				for _, wp := range rf.wrapPeers {
					if wp.serverID != rf.me {
						args := &RequestVoteArgs{Term: rf.currentTerm, CandidateID: rf.me,
							LastLogTerm: rf.log.LastTerm, LastLogIndex: rf.log.LastIndex}
						wp.voteChan <- &VoteEvent{args: args, reply: &RequestVoteReply{}}
					}
				}
			}
			rf.mux.Unlock()
		}
		rf.electionTimer.Reset(time.Duration(GetElectionTimeout(rf.electionTimeout)) * time.Millisecond)
	}
}

func meVoteLoop(rf *Raft) {
	for !checkLoopStop(rf) {
		rf.mux.Lock()
		peerNum := len(rf.peers)
		voteNum := 0
		for i := 0; i < peerNum-1; i++ {
			ve := <-rf.voteResultChan
			if ve.suc {
				if ve.reply.VoteGranted {
					voteNum++
				}
				rf.logger.Printf("Raft %d receive vote %s", rf.me, ve.reply)
				rf.currentTerm = ve.reply.Term
			}
		}
		if voteNum >= peerNum%2 {
			oldRole := rf.role
			rf.role = Leader
			if oldRole != Leader {
				go heartbeatLoop(rf)
			}
		}
		rf.mux.Unlock()

	}
}

func peerVoteLoop(rf *Raft, peer *Peer) {
	for !checkLoopStop(rf) {
		ve := <-peer.voteChan
		ve.suc = rf.sendRequestVote(peer.serverID, ve.args, ve.reply)
		rf.voteResultChan <- ve
	}
}

func heartbeatLoop(rf *Raft) {
	heartbeatTimer := time.NewTimer(0)
	for {
		<-heartbeatTimer.C
		rf.mux.Lock()
		if rf.role != Leader {
			break
		}
		for _, wp := range rf.wrapPeers {
			if wp.serverID != rf.me {
				args := &AppendEntriesArgs{Term: rf.currentTerm, LeaderID: wp.serverID}
				rf.sendAppendEntries(wp.serverID, args, &AppendEntriesReply{})
			}
		}
		rf.mux.Unlock()
		heartbeatTimer.Reset(time.Duration(rf.heartbeatInverval) * time.Millisecond)
	}
}

func checkLoopStop(rf *Raft) bool {
	return false
}
