package raft

import (
	"bufio"
	"bytes"
	"container/list"
	"encoding/binary"
	"encoding/gob"
	"errors"
	"fmt"
	"io"
	"os"
)

// Entry Log文件中log
type Entry struct {
	Term  int
	Index int
	Data  []byte
}

// PersistentLog log文件实现
type PersistentLog struct {
	dir       string
	fileName  string
	file      *os.File
	rw        *bufio.ReadWriter
	entries   *list.List // log
	LastTerm  int        // 最新的term
	LastIndex int        // 最新的index
}

//NewPersistentLog 新建
func NewPersistentLog(dir string, fileName string) *PersistentLog {
	return &PersistentLog{
		dir:      dir,
		fileName: fileName,
		entries:  list.New(),
	}
}

//Init 初始化log，将磁盘中的信息加载到内存中
func (p *PersistentLog) Init() error {
	err := os.MkdirAll(p.dir, os.ModePerm)
	if err != nil {
		return err
	}
	file, err := os.OpenFile(p.dir+p.fileName, os.O_RDWR|os.O_CREATE, os.ModePerm)
	if err != nil {
		file.Close()
		return err
	}
	p.file = file
	rw := bufio.NewReadWriter(bufio.NewReader(file), bufio.NewWriter(file))
	p.rw = rw

	for {
		bs, err := rw.ReadBytes('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			file.Close()
			return err
		}
		index := int(binary.LittleEndian.Uint32(bs[0:8]))
		term := int(binary.LittleEndian.Uint64(bs[8:16]))
		if term < p.LastTerm {
			return errors.New("Log file term error")
		}
		if index <= p.LastIndex {
			return errors.New("Log file index error")
		}
		p.LastTerm = term
		p.LastIndex = index
		end := len(bs) - 1
		p.entries.PushBack(&Entry{
			Term:  term,
			Index: index,
			Data:  bs[16:end],
		})
	}
	return nil
}

// AppendLog 追加log内容
func (p *PersistentLog) AppendLog(term int, ac *ApplyCommand) error {
	if term < p.LastTerm {
		return errors.New("Term < lastTerm")
	}
	if ac.Index <= p.LastIndex {
		return errors.New("Index <= lastIndex")
	}
	buf := make([]byte, 8)
	binary.LittleEndian.PutUint32(buf, uint32(ac.Index))
	nn, err := p.rw.Write(buf)
	if err != nil && nn == 0 {
		return err
	}
	binary.LittleEndian.PutUint32(buf, uint32(term))
	nn, err = p.rw.Write(buf)
	if err != nil && nn == 0 {
		return err
	}
	qb := new(bytes.Buffer)
	qe := gob.NewEncoder(qb)
	qe.Encode(ac.Command)
	command := qb.Bytes()
	nn, err = p.rw.Write(command)
	if err != nil && nn == 0 {
		return err
	}
	p.rw.Flush()
	p.entries.PushBack(&Entry{Term: term, Index: ac.Index, Data: command})
	return nil
}

// Close 关闭
func (p *PersistentLog) Close() error {
	return p.file.Close()
}

func (p *PersistentLog) CheckPre(prevLogTerm int, prevLogIndex int) bool {
	for f := p.entries.Front(); f != nil; f = f.Next() {
		entry := f.Value.(*Entry)
		if entry.Index == prevLogIndex && entry.Term == prevLogTerm {
			return true
		}
	}
	return false
}

func (p *PersistentLog) String() string {
	entryFormat := "[%d %d %s]"
	s := ""
	for f := p.entries.Front(); f != nil; f = f.Next() {
		entry := f.Value.(*Entry)
		s += fmt.Sprintf(entryFormat, entry.Term, entry.Index, string(entry.Data))
		if f.Next() != nil {
			s += "\n"
		}
	}
	return s
}
