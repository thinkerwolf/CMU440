package main

import (
	"container/list"
	"encoding/json"
	"fmt"
	"hash/crc32"
	"math/rand"
	"strconv"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"github.com/cmu440/lsp"
)

func TestHeap(t *testing.T) {
	connId := 20
	pay := []byte("253000")
	var payload int
	json.Unmarshal(pay, &payload)
	longerPayload, _ := json.Marshal(payload * 1000)
	t.Errorf("Normal %d, Longer %d \n", lsp.CalChecksum(connId, pay), lsp.CalChecksum(connId, longerPayload))
	t.Error("Test suc")
}

func TestMarshalling(t *testing.T) {
	payload := []byte("value_8_31")
	msg := lsp.NewData(99999, 1, len(payload), payload, lsp.CalChecksum(1, payload))
	t.Log("Marshal:", msg)
	bs, err := json.Marshal(msg)
	if err != nil {
		t.Errorf("Test error %s", err)
		return
	}
	//var m lsp.Message
	t.Log("string ", string(bs))
	m := new(lsp.Message)
	json.Unmarshal(bs, m)
	t.Log("Unmarshal:", m)

	t.Errorf("Test success")
}

func TestAtomic(t *testing.T) {
	var num int32 = 1
	n := atomic.AddInt32(&num, 1)

	t.Log("Atomic n: ", n)
	t.Log("Num:", num)
	t.Errorf("Test success")
}

func TestHash(t *testing.T) {
	v := int(crc32.ChecksumIEEE([]byte("cvda")))
	t.Errorf("Hash code %d ", v)
}

func TestLock(t *testing.T) {
	in := make(chan int, 10)
	lock := new(sync.Mutex)
	notEmpty := sync.NewCond(lock)
	notFull := sync.NewCond(lock)
	notFull = notEmpty
	go func() {
		num := 1
		for {
			lock.Lock()
			for len(in) >= 10 {
				notFull.Wait()
			}
			// 产生阻塞
			in <- num
			t.Log("Producer put ", num)
			num++
			notEmpty.Signal()
			lock.Unlock()

			time.Sleep(time.Duration(50) * time.Millisecond)
		}
	}()

	go func() {
		for {
			lock.Lock()
			for len(in) == 0 {
				notEmpty.Wait()
			}
			num := <-in
			t.Log("Consumer get ", num)
			notFull.Signal()
			lock.Unlock()
			time.Sleep(time.Duration(200) * time.Millisecond)
		}
	}()

	time.Sleep(time.Duration(10000) * time.Millisecond)
	t.Errorf("Test success")
}

func TestMap(t *testing.T) {
	m := make(map[int]string)
	for i := 0; i < 10; i++ {
		m[i] = "value" + strconv.Itoa(i)
	}

	for i := 0; i < 10; i++ {
		if i%2 == 0 {
			delete(m, i)
		}
	}
	b, _ := json.Marshal(m)
	t.Errorf("map : %s", string(b))
}

func TestList(t *testing.T) {
	l := list.New()
	for i := 0; i < 10; i++ {
		l.PushBack(lsp.NewAck(i, 0))
	}

	for f := l.Front(); f != nil; {
		msg := f.Value.(*lsp.Message)
		t.Logf("%s\n", msg)

		removed := f
		f = removed.Next()
		if msg.ConnID%2 == 0 {
			l.Remove(removed)
		}
	}
	t.Logf("%s\n", "=================================")
	for f := l.Front(); f != nil; {
		msg := f.Value.(*lsp.Message)
		t.Logf("%s\n", msg)
		removed := f
		f = removed.Next()
	}

	t.Error("Test success")
}

func TestServer(t *testing.T) {
	port := 9001
	err := runLspTest(t, port, 20, 100, 100, 20000)
	if err != nil {
		t.Errorf("Start server error %s", err)
	}
	//t.Error("Test success")
}

func runLspTest(t *testing.T, port int, clientNum int, messages int, maxClientWriteInterval int, timeout int) error {
	serv, err := lsp.NewServer(port, lsp.NewParams())
	if err != nil {
		return err
	}
	time.Sleep(time.Duration(100) * time.Millisecond) // 等待服务器启动完成

	hostport := "127.0.0.1:" + strconv.Itoa(port)
	clients := make([]lsp.Client, clientNum)
	for i := 0; i < len(clients); i++ {
		c, err := lsp.NewClient(hostport, lsp.NewParams())
		if err != nil {
			return err
		}
		clients[i] = c
	}
	time.Sleep(time.Duration(1000) * time.Millisecond) // 等待客户端启动连接完成

	serverReadMsgNum := int32(0)
	serverWriteMsgNum := int32(0)
	clientsReadMsgNum := int32(0)
	clientsWriteMsgNum := int32(0)

	// 启动服务端读写
	servWriteIndex := int32(0)
	for i := 0; i < 4; i++ {
		go func() {
			for {
				//time.Sleep(time.Duration(50) * time.Millisecond)
				connID, payload, err := serv.Read()
				if err == nil {
					fmt.Printf("Server read data :%s\n", string(payload))
				}
				atomic.AddInt32(&serverReadMsgNum, 1)
				serv.Write(connID, []byte(fmt.Sprintf("serverValue_%d %s", atomic.AddInt32(&servWriteIndex, 1), string(payload))))
				atomic.AddInt32(&serverWriteMsgNum, 1)
			}
		}()
	}

	// 启动客户端读写
	for _, v := range clients {
		cli := v
		go func() {
			for msgID := 0; msgID < messages; msgID++ {
				value := fmt.Sprintf("clientValue_%d_%d", cli.ConnID(), msgID+1)
				cli.Write([]byte(value))
				atomic.AddInt32(&clientsWriteMsgNum, 1)
				time.Sleep(time.Duration(rand.Intn(maxClientWriteInterval)+1) * time.Millisecond)
			}
		}()

		go func() {
			for {
				time.Sleep(time.Duration(100) * time.Millisecond)
				payload, err := cli.Read()
				if err != nil {
					break
				}
				atomic.AddInt32(&clientsReadMsgNum, 1)
				fmt.Printf("Client read data :%s\n", string(payload))
			}
		}()

		// go func() {
		// 	time.Sleep(time.Duration(rand.Intn(100)+50) * time.Millisecond)
		// 	cli.Close()
		// }()
	}
	time.Sleep(time.Duration(timeout) * time.Millisecond)

	fmt.Printf("Server read num %d\n", serverReadMsgNum)
	fmt.Printf("Server write num %d\n", serverWriteMsgNum)
	fmt.Printf("Clients read num %d\n", clientsReadMsgNum)
	fmt.Printf("Clients write num %d\n", clientsWriteMsgNum)
	return nil
}
