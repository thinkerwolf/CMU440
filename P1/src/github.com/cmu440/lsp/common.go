package lsp

import (
	"container/list"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"hash/crc32"
	"log"
	"os"
	"sync"
	"sync/atomic"
	"time"

	"github.com/cmu440/lspnet"
)

const (
	// MsgClose 关闭连接
	MsgClose MsgType = MsgAck + 1
	// BUFFSIZE 读取缓冲大小
	BUFFSIZE int = 1024 * 4
)

var (
	//StateIni 初始化状态
	StateIni int32 = 0
	//StateRunning 正常运行状态
	StateRunning int32 = 1
	//StateShutdown 关闭状态，不接受写，所有的写任务必须完成
	StateShutdown int32 = 2
	//StateTerminated 终止状态，不接受写，
	StateTerminated int32 = 3
)

var (
	servLogger     = log.New(os.Stdout, "[lsp-server] ", log.LstdFlags)
	cliLogger      = log.New(os.Stdout, "[lsp-client] ", log.LstdFlags)
	debugLogEnable uint32
)

//ReadPack 读取的消息包装
type ReadPack struct {
	Msg *Message
	err error
}

//WritePack 消息包装
type WritePack struct {
	CurrentBackOff int
	NextEpoch      int
	startEpoch     int
	acked          bool
	Msg            *Message
}

//WriteManager 写消息管理器
type WriteManager struct {
	waitPacks  *list.List //待发送的消息pack
	unackPacks *list.List //未响应消息pack
}

// NewWriteManager new manager
func NewWriteManager() *WriteManager {
	return &WriteManager{
		waitPacks:  list.New(),
		unackPacks: list.New(),
	}
}

// AddWaitPack 添加待发送消息pack
func (m *WriteManager) AddWaitPack(pack *WritePack) *list.Element {
	var mark *list.Element
	for f := m.waitPacks.Back(); f != nil; f = f.Prev() {
		p := f.Value.(*WritePack)
		if pack.Msg.SeqNum == p.Msg.SeqNum {
			return nil
		}
		if pack.Msg.SeqNum >= p.Msg.SeqNum {
			mark = f
			break
		}
	}
	if mark != nil {
		return m.waitPacks.InsertAfter(pack, mark)
	}
	return m.waitPacks.PushFront(pack)
}

// AddUnackPack 未响应消息pack
func (m *WriteManager) AddUnackPack(pack *WritePack) *list.Element {
	var mark *list.Element
	for f := m.unackPacks.Back(); f != nil; f = f.Prev() {
		p := f.Value.(*WritePack)
		if pack.Msg.SeqNum == p.Msg.SeqNum {
			return nil
		}
		if pack.Msg.SeqNum > p.Msg.SeqNum {
			mark = f
			break
		}
	}
	if mark != nil {
		return m.unackPacks.InsertAfter(pack, mark)
	}
	return m.unackPacks.PushFront(pack)
}

// HandleAck 处理响应消息
func (m *WriteManager) HandleAck(seqNum int) {
	f := m.unackPacks.Front()
	if f == nil {
		return
	}
	pack := f.Value.(*WritePack)
	if seqNum == pack.Msg.SeqNum {
		pack.acked = true
		sq := seqNum
		for f != nil {
			fn := f.Next()
			pack = f.Value.(*WritePack)
			if sq == pack.Msg.SeqNum && pack.acked {
				m.unackPacks.Remove(f)
			} else {
				break
			}
			f = fn
			sq++
		}
	} else {
		for ; f != nil; f = f.Next() {
			pack = f.Value.(*WritePack)
			if seqNum == pack.Msg.SeqNum {
				pack.acked = true
				break
			}
		}
	}
}

// CheckWriteOver 检查消息是否发送完毕
func (m *WriteManager) CheckWriteOver() bool {
	unackOver := true
	for f := m.unackPacks.Front(); f != nil; f = f.Next() {
		if !f.Value.(*WritePack).acked {
			unackOver = false
			break
		}
	}
	if m.waitPacks.Len() == 0 && unackOver {
		return true
	}
	return false
}

// CheckWritable 检查消息是否可以发送
func (m *WriteManager) CheckWritable(params *Params) bool {
	if m.waitPacks.Len() == 0 || m.unackPacks.Len() >= params.WindowSize {
		return false
	}
	ackNum := 0
	for f := m.unackPacks.Front(); f != nil; f = f.Next() {
		if f.Value.(*WritePack).acked {
			ackNum++
		}
	}
	if m.unackPacks.Len()-ackNum >= params.MaxUnackedMessages {
		return false
	}
	return true
}

// CheckClosable 检查连接是否可关闭
func (m *WriteManager) CheckClosable(params *Params, epoch int) bool {
	for f := m.unackPacks.Front(); f != nil; f = f.Next() {
		v := f.Value.(*WritePack)
		if v.startEpoch < 0 {
			v.startEpoch = epoch
			v.NextEpoch = epoch + v.CurrentBackOff
		}
		if epoch-v.startEpoch >= params.EpochLimit {
			return true
		}
	}
	return false
}

// TryRewrite 尝试重新发送未ack消息
func (m *WriteManager) TryRewrite(netConn *lspnet.UDPConn, addr *lspnet.UDPAddr, params *Params, epoch int) {
	for f := m.unackPacks.Front(); f != nil; {
		fn := f.Next()
		v := f.Value.(*WritePack)
		if epoch >= v.NextEpoch && !v.acked {
			v.CurrentBackOff = NextBackOff(v, params.MaxBackOffInterval)
			v.NextEpoch = epoch + v.CurrentBackOff
			servLogger.Printf("Retry epoch:%d %s \n", epoch, MsgPackToString(v))
			//m.AddWaitPack(v)
			//m.unackPacks.Remove(f)
			writeMsg(netConn, addr, v.Msg)
		}
		f = fn
	}
}

// PollWritPack 拉取一条待写包
func (m *WriteManager) PollWritPack() *WritePack {
	f := m.waitPacks.Front()
	if f == nil {
		return nil
	}
	pack := f.Value.(*WritePack)
	m.waitPacks.Remove(f)
	return pack
}

type connection struct {
	connID          int
	Addr            *lspnet.UDPAddr
	writeCond       *sync.Cond
	writeManager    *WriteManager
	seqNumGenerator int32 // 消息序列号
	lastReadSepNum  int32 // 上一次读取的序列号
	state           int32 // 连接状态
	readCond        *sync.Cond
	readMsgs        *list.List // 客户端收到的消息
	epoch           int
	epochTimer      *time.Timer
	heartbeat       *Heartbeat
}

func NewConnection(connID int, addr *lspnet.UDPAddr, state int32) *connection {
	return &connection{
		Addr:         addr,
		connID:       connID,
		writeCond:    sync.NewCond(new(sync.Mutex)),
		state:        state,
		writeManager: NewWriteManager(),
		readCond:     sync.NewCond(new(sync.Mutex)),
		readMsgs:     list.New(),
		epochTimer:   time.NewTimer(0),
		heartbeat:    NewHeartbeat(),
	}
}

func (c *connection) addReadMsgs(msg *Message) *list.Element {
	add := true
	if msg.Type == MsgData && msg.SeqNum <= int(atomic.LoadInt32(&c.lastReadSepNum)) {
		add = false
	}
	if add {
		return c.readMsgs.PushBack(&ReadPack{Msg: msg})
	}
	return nil
}

func (c *connection) pollReadMsg() *Message {
	lsq := int(atomic.LoadInt32(&c.lastReadSepNum))
	for f := c.readMsgs.Front(); f != nil; {
		pack := f.Value.(*ReadPack)
		next := f.Next()
		if pack.err == nil {
			if pack.Msg.Type == MsgClose {
				c.readMsgs.Remove(f)
				return pack.Msg
			} else if lsq+1 == pack.Msg.SeqNum {
				c.readMsgs.Remove(f)
				atomic.StoreInt32(&c.lastReadSepNum, int32(pack.Msg.SeqNum))
				return pack.Msg
			}
		}
		f = next
	}
	return nil
}

func (c *connection) readOver() bool {
	c.readCond.L.Lock()
	defer c.readCond.L.Unlock()
	return c.readMsgs.Len() == 0
}

func (c *connection) writeOver() bool {
	c.writeCond.L.Lock()
	defer c.writeCond.L.Unlock()
	return c.writeManager.CheckWriteOver()
}

type Heartbeat struct {
	epoch       int32 // 发送heartbeat时的epoch数
	writeMsgCnt int32 // 上一轮epoch写的信息数量
}

func NewHeartbeat() *Heartbeat {
	return &Heartbeat{
		epoch:       0,
		writeMsgCnt: 0,
	}
}

func (h *Heartbeat) isTimeout(epoch int, max int) bool {
	return epoch-int(atomic.LoadInt32(&h.epoch)) >= max
}

func (h *Heartbeat) addWriteCnt() {
	atomic.AddInt32(&h.writeMsgCnt, 1)
}

func (h *Heartbeat) setWriteCnt(n int) {
	atomic.StoreInt32(&h.writeMsgCnt, int32(n))
}

func (h *Heartbeat) setEpoch(epoch int) {
	atomic.StoreInt32(&h.epoch, int32(epoch))
}

func (h *Heartbeat) get() (int, int) {
	return int(atomic.LoadInt32(&h.writeMsgCnt)), int(atomic.LoadInt32(&h.epoch))
}

// ===============================

//Checksum 计算checksum
func Checksum(msg *Message) uint16 {
	ck := Int2Checksum(int(msg.Type))
	ck += Int2Checksum(msg.ConnID)
	ck += Int2Checksum(int(ck))
	ck += Int2Checksum(msg.SeqNum)
	ck += Int2Checksum(int(ck))
	ck += ByteArray2Checksum(msg.Payload)

	buf := make([]byte, 4)
	binary.LittleEndian.PutUint32(buf, ck)
	return binary.LittleEndian.Uint16(buf[0:2])
}

//CalChecksum 计算checksum
func CalChecksum(connID int, payload []byte) uint16 {
	ck := Int2Checksum(connID)
	ck += Int2Checksum(int(ck))
	ck += ByteArray2Checksum(payload)
	ck += Int2Checksum(int(ck))

	buf := make([]byte, 4)
	binary.LittleEndian.PutUint32(buf, ck)
	return binary.LittleEndian.Uint16(buf[0:2])
}

//NewSeqNum 生成消息序列号
func NewSeqNum(addr *int32) int {
	sq := int(atomic.AddInt32(addr, 1))
	if sq < 0 {
		return -sq
	}
	return sq
}

//NextBackOff 下一次重试epoch偏移
func NextBackOff(msgPack *WritePack, maxBackOffInterval int) int {
	currentBackOff := msgPack.CurrentBackOff
	if currentBackOff < maxBackOffInterval {
		currentBackOff *= 2
		if currentBackOff > maxBackOffInterval {
			currentBackOff = maxBackOffInterval
		}
	}
	if currentBackOff <= 0 {
		return 1
	}
	return currentBackOff
}

//MsgPackToString MsgPack转string
func MsgPackToString(msgPack *WritePack) string {
	return fmt.Sprintf("StartEpoch %d, NextEpoch %d, CurrentBackOff %d, Msg %s", msgPack.startEpoch, msgPack.NextEpoch, msgPack.CurrentBackOff, msgPack.Msg.String())
}

// GetConnectionID 根据地址字符串获取连接ID
func GetConnectionID(addr *lspnet.UDPAddr) int {
	return int(crc32.ChecksumIEEE([]byte(addr.String())))
}

//NewClose 创建Close信息
func NewClose(connID, seqNum int) *Message {
	return &Message{
		Type:   MsgClose,
		ConnID: connID,
		SeqNum: seqNum,
	}
}

//writeMsg 往服务端或者客户端写Message消息
func writeMsg(conn *lspnet.UDPConn, raddr *lspnet.UDPAddr, msg *Message) (int, error) {
	data, err := json.Marshal(msg)
	if err != nil {
		return 0, err
	}
	if raddr != nil {
		return conn.WriteToUDP(data, raddr)
	}
	return conn.Write(data)
}

func readMsg(conn *lspnet.UDPConn, buf []byte, isServer bool) (*Message, *lspnet.UDPAddr, error) {
	var addr *lspnet.UDPAddr
	var n int
	var err error
	if isServer {
		n, addr, err = conn.ReadFromUDP(buf)
	} else {
		n, err = conn.Read(buf)
	}
	if err != nil {
		return nil, addr, err
	}
	msg := NewAck(0, 0)
	err = json.Unmarshal(buf[:n], &msg)
	if err != nil {
		return nil, addr, err
	}
	return msg, addr, err
}

func notifyReadAndWrite(readCond *sync.Cond, writeCond *sync.Cond) {
	if readCond != nil {
		readCond.L.Lock()
		readCond.Signal()
		readCond.L.Unlock()
	}
	if writeCond != nil {
		writeCond.L.Lock()
		writeCond.Signal()
		writeCond.L.Unlock()
	}
}

func isDebugLogEnable() bool {
	return atomic.LoadUint32(&debugLogEnable) > 0
}

//SetDebugLogEnable debug日志是否开启
func SetDebugLogEnable(enable bool) {
	if enable {
		atomic.StoreUint32(&debugLogEnable, 1)
	} else {
		atomic.StoreUint32(&debugLogEnable, 0)
	}
}
