package lsp

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/cmu440/lspnet"
)

type testReadEvent struct {
	data []byte
}

type simpleclient struct {
	conn   *lspnet.UDPConn
	sq     int32
	connID int
}

func NewSimpleClient(hostport string, params *Params) (Client, error) {
	// lspnet.
	laddr, err := lspnet.ResolveUDPAddr("udp", "0.0.0.0:0")
	if err != nil {
		return nil, err
	}
	raddr, err := lspnet.ResolveUDPAddr("udp", hostport)
	if err != nil {
		return nil, err
	}
	conn, err := lspnet.DialUDP("udp", laddr, raddr)
	if err != nil {
		return nil, err
	}
	c := &simpleclient{
		conn: conn,
	}
	go func() {
		for {
			buf := make([]byte, 4096)
			n, err := c.conn.Read(buf)
			if err != nil {
				break
			}
			data := buf[0:n]
			process(c, data)
		}
	}()
	writeConnect(c)
	return c, nil
}

func process(c *simpleclient, data []byte) {
	msg := new(Message)
	json.Unmarshal(data, msg)
	switch msg.Type {
	case MsgAck:
		if msg.SeqNum == 0 {
			c.connID = msg.ConnID
		}
		fmt.Printf("Client received ack %s\n", msg)
	case MsgData:
		fmt.Printf("Client received data %s\n", msg)

		ack := NewAck(msg.ConnID, msg.SeqNum)

		bs, _ := json.Marshal(ack)
		c.conn.Write(bs)
	}
}

func (c *simpleclient) ConnID() int {
	return c.connID
}

func (c *simpleclient) Read() ([]byte, error) {
	return nil, errors.New("not yet implemented")
}

func (c *simpleclient) Write(payload []byte) error {
	seqNum := NewSeqNum(&(c.sq))
	msg := NewData(c.ConnID(), seqNum, len(payload), payload, CalChecksum(c.ConnID(), payload))
	data, err := json.Marshal(msg)
	if err != nil {
		return err
	}
	n, err := c.conn.Write(data)
	if n == 0 || err != nil {
		return err
	}
	return nil
}

func (c *simpleclient) Close() error {
	return c.conn.Close()
}

func writeConnect(c *simpleclient) error {
	msg := NewConnect()
	data, err := json.Marshal(msg)
	if err != nil {
		return err
	}
	n, err := c.conn.Write(data)
	if n == 0 || err != nil {
		return err
	}
	return nil
}
