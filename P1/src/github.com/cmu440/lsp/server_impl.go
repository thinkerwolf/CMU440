// Contains the implementation of a LSP server.

package lsp

import (
	"container/list"
	"errors"
	"fmt"
	"strconv"
	"sync"
	"sync/atomic"
	"time"

	"github.com/cmu440/lspnet"
)

type readEvent struct {
	data []byte
	addr *lspnet.UDPAddr
	err  error
}

type server struct {
	// TODO: Implement this!
	params         *Params
	conn           *lspnet.UDPConn
	connectionMap  map[int]*connection // 连接信息
	connectionLock *sync.Mutex
	//readMsgs      *list.List
	readConnIds *list.List
	queueLock   *sync.Mutex
	readCond    *sync.Cond
	state       int32
	stateCond   *sync.Cond
	closeChan   chan bool
}

// NewServer creates, initiates, and returns a new server. This function should
// NOT block. Instead, it should spawn one or more goroutines (to handle things
// like accepting incoming client connections, triggering epoch events at
// fixed intervals, synchronizing events using a for-select loop like you saw in
// project 0, etc.) and immediately return. It should return a non-nil error if
// there was an error resolving or listening on the specified port number.
func NewServer(port int, params *Params) (Server, error) {
	addr := "0.0.0.0:" + strconv.Itoa(port)
	laddr, err := lspnet.ResolveUDPAddr("udp", addr)
	if err != nil {
		return nil, err
	}
	conn, err := lspnet.ListenUDP("udp", laddr)
	if err != nil {
		return nil, err
	}
	serv := &server{
		params:         params,
		conn:           conn,
		connectionMap:  make(map[int]*connection),
		connectionLock: new(sync.Mutex),
		readCond:       sync.NewCond(new(sync.Mutex)),
		//readMsgs:      list.New(),
		readConnIds: list.New(),
		state:       StateRunning,
		stateCond:   sync.NewCond(new(sync.Mutex)),
		closeChan:   make(chan bool),
	}
	go servReadLoop(serv)
	servLogger.Printf("Listen on port : %d success\n", port)
	return serv, nil
}

func (s *server) Read() (int, []byte, error) {
	s.readCond.L.Lock()
	for {
		// Server关闭
		if atomic.LoadInt32(&s.state) >= StateShutdown {
			s.readCond.L.Unlock()
			return 0, nil, errors.New("Server close")
		}
		for f := s.readConnIds.Front(); f != nil; f = f.Next() {
			connID := f.Value.(int)
			c := getConn(s, connID)
			if c != nil {
				msg := c.pollReadMsg()
				if msg != nil {
					s.readConnIds.Remove(f)
					s.readCond.L.Unlock()
					checkAndCloseConn(s, c, 1)
					if msg.Type == MsgClose {
						return c.connID, nil, errors.New("Client close")
					}
					return c.connID, msg.Payload, nil
				}
			}
		}
		s.readCond.Wait()
	}
}

func (s *server) Write(connID int, payload []byte) error {
	if atomic.LoadInt32(&s.state) >= StateShutdown {
		return errors.New("Server close, can't write")
	}
	ok, c := checkConnRunning(s, connID)
	if !ok {
		return fmt.Errorf("Connection does't exist %d", connID)
	}
	msg := NewData(connID, NewSeqNum(&c.seqNumGenerator), len(payload), payload, CalChecksum(connID, payload))
	c.writeCond.L.Lock()
	c.writeManager.AddWaitPack(&WritePack{Msg: msg, startEpoch: -1})
	c.writeCond.Signal()
	c.writeCond.L.Unlock()
	return nil
}

func (s *server) CloseConn(connID int) error {
	_, c := checkConnRunning(s, connID)
	if c != nil {
		atomic.StoreInt32(&c.state, StateShutdown)
		s.readCond.L.Lock()
		s.readConnIds.PushBack(c.connID)
		c.addReadMsgs(NewClose(connID, 1))
		s.readCond.Signal()
		s.readCond.L.Unlock()

		c.writeCond.L.Lock()
		c.writeCond.Signal()
		c.writeCond.L.Unlock()
		checkAndCloseConn(s, c, 1)
		return nil
	}
	return errors.New("Conn closed already")
}

func (s *server) Close() error {
	// 等待所有的连接写完
	if atomic.LoadInt32(&s.state) >= StateShutdown {
		return errors.New("Server already closed")
	}
	atomic.StoreInt32(&s.state, StateShutdown)
	if checkServTerminated(s) {
		atomic.StoreInt32(&s.state, StateTerminated)
		s.conn.Close()
		notifyReadAndWrite(s.readCond, nil)
		if isDebugLogEnable() {
			servLogger.Println("**** Server close finish ****")
		}
		return nil
	}
	for {
		select {
		case cmd := <-s.closeChan:
			if cmd {
				atomic.StoreInt32(&s.state, StateTerminated)
				s.conn.Close()
				notifyReadAndWrite(s.readCond, nil)
				if isDebugLogEnable() {
					servLogger.Println("****** Server close finish ******")
				}
				return nil
			}
		}
	}
}

// servReadLoop 服务器读取
func servReadLoop(s *server) {
	for {
		if atomic.LoadInt32(&s.state) >= StateTerminated {
			break
		}
		buf := make([]byte, 1024*4) // buf 4K
		msg, addr, err := readMsg(s.conn, buf, true)
		if err != nil {
			servLogger.Printf("Error occur when read message %s\n", err)
		} else {
			if isDebugLogEnable() {
				servLogger.Printf("Receive message %s\n", msg)
			}
			switch msg.Type {
			case MsgConnect:
				// 连接
				c := getOrCreateConn(s, addr)
				c.heartbeat.setEpoch(c.epoch)
				writeMsg(s.conn, addr, NewAck(c.connID, 0))
			case MsgAck:
				// 接收ack
				c := getConn(s, msg.ConnID)
				if c == nil {
					servLogger.Printf("Connection is removed, but receive msg %s\n", msg)
					continue
				}
				c.heartbeat.setEpoch(c.epoch)

				c.writeCond.L.Lock()
				c.writeManager.HandleAck(msg.SeqNum)
				if c.writeManager.CheckWritable(s.params) {
					c.writeCond.Signal()
				}
				c.writeCond.L.Unlock()
				// Check write finish
				if atomic.LoadInt32(&s.state) == StateShutdown && checkServTerminated(s) {
					s.closeChan <- true
				}
				// Check conn write finish
				checkAndCloseConn(s, c, 1)
			case MsgData:
				// 数据信息
				c := getConn(s, msg.ConnID)
				if c == nil {
					servLogger.Printf("Connection is removed, but receive msg %s\n", msg)
					continue
				}
				c.heartbeat.setEpoch(c.epoch)
				// 消息进行校验
				calChecksum := CalChecksum(msg.ConnID, msg.Payload)
				if calChecksum != msg.Checksum {
					servLogger.Printf("Checksum incorrect %s, checksum %d", msg.String(), calChecksum)
					continue
				}
				s.readCond.L.Lock()
				if c.addReadMsgs(msg) != nil {
					s.readConnIds.PushBack(c.connID)
					s.readCond.Signal()
				}
				s.readCond.L.Unlock()
				writeMsg(s.conn, c.Addr, NewAck(c.connID, msg.SeqNum))
			default:
			}
		}
	}
}

func connWriteLoop(s *server, c *connection) {
	writeCond := c.writeCond
	for {
		if checkWriteStop(s, c) {
			break
		}
		writeCond.L.Lock()
		for !c.writeManager.CheckWritable(s.params) {
			writeCond.Wait()
		}
		if checkWriteStop(s, c) {
			writeCond.L.Unlock()
			break
		}
		pack := c.writeManager.PollWritPack()
		_, err := writeMsg(s.conn, c.Addr, pack.Msg)
		c.writeManager.AddUnackPack(pack)
		c.heartbeat.addWriteCnt()
		writeCond.L.Unlock()
		if err != nil {
			servLogger.Printf("Write msg fail %s\n", err)
			break
		}
		if isDebugLogEnable() {
			servLogger.Printf("Write msg success %s\n", pack.Msg)
		}
	}
}

func epoch(s *server, c *connection) {
	c.epochTimer.Reset(time.Duration(s.params.EpochMillis) * time.Millisecond)
	c.epoch = 0
	for {
		<-c.epochTimer.C
		if checkEpochStop(s, c) {
			break
		}
		// 一直没有读取到消息。 c.params.EpochLimit * c.params.EpochMillis
		if c.heartbeat.isTimeout(c.epoch, s.params.EpochLimit) {
			atomic.StoreInt32(&c.state, StateShutdown)
			s.CloseConn(c.connID)
			if isDebugLogEnable() {
				servLogger.Printf("Epoch close connection %d\n", c.connID)
			}
			break
		}
		// 如果上一轮epoch没有发送任何消息，发heartbeat
		wm, _ := c.heartbeat.get()
		if wm == 0 {
			writeMsg(s.conn, c.Addr, NewAck(c.connID, 0)) // heartbeat
		}
		c.heartbeat.setWriteCnt(0)
		// 重发未ack的消息
		c.writeCond.L.Lock()
		shouldClose := c.writeManager.CheckClosable(s.params, c.epoch)
		if !shouldClose {
			c.writeManager.TryRewrite(s.conn, c.Addr, s.params, c.epoch)
			c.writeCond.Signal()
		}
		c.writeCond.L.Unlock()

		if shouldClose {
			atomic.StoreInt32(&c.state, StateShutdown)
			s.CloseConn(c.connID)
			if isDebugLogEnable() {
				servLogger.Printf("Epoch close connection %d\n", c.connID)
			}
			break
		}
		c.epoch++
		c.epochTimer.Reset(time.Duration(s.params.EpochMillis) * time.Millisecond)
	}
}

func getOrCreateConn(s *server, addr *lspnet.UDPAddr) *connection {
	connID := GetConnectionID(addr)
	s.connectionLock.Lock()
	c, ok := s.connectionMap[connID]
	if !ok {
		c = NewConnection(connID, addr, StateRunning)
		s.connectionMap[connID] = c
		go epoch(s, c)
		go connWriteLoop(s, c)
	}
	s.connectionLock.Unlock()
	return c
}

func getConn(s *server, connID int) *connection {
	s.connectionLock.Lock()
	conn := s.connectionMap[connID]
	s.connectionLock.Unlock()
	return conn
}

func getConnState(c *connection) int32 {
	if c == nil {
		return -1
	}
	return atomic.LoadInt32(&c.state)
}

func checkConnRunning(s *server, connID int) (bool, *connection) {
	conn := getConn(s, connID)
	return getConnState(conn) == StateRunning, conn
}

// 检查所有的写任务完成
func checkServTerminated(s *server) bool {
	s.connectionLock.Lock()
	ok := true
	for _, c := range s.connectionMap {
		if !c.writeOver() {
			ok = false
			break
		}
	}
	defer s.connectionLock.Unlock()
	return ok
}

func checkEpochStop(s *server, c *connection) bool {
	if atomic.LoadInt32(&s.state) >= StateTerminated {
		return true
	}
	if c == nil {
		return true
	}
	ok, _ := checkConnRunning(s, c.connID)
	return !ok
}

func checkWriteStop(s *server, c *connection) bool {
	if atomic.LoadInt32(&s.state) >= StateTerminated {
		return true
	}
	if c == nil {
		return true
	}
	c = getConn(s, c.connID)
	if c == nil {
		return true
	}
	return c.state > StateShutdown
}

func checkAndCloseConn(s *server, c *connection, seqNum int) bool {
	state := atomic.LoadInt32(&c.state)
	if c == nil && state >= StateTerminated {
		return true
	}
	if state != StateShutdown {
		return false
	}
	writeOver := c.writeOver()
	readOver := c.readOver()
	if writeOver && readOver {
		s.connectionLock.Lock()
		atomic.StoreInt32(&c.state, StateTerminated)
		delete(s.connectionMap, c.connID)
		s.connectionLock.Unlock()
		return true
	}
	return false
}
