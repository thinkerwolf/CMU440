// Contains the implementation of a LSP client.

package lsp

import (
	"container/list"
	"errors"
	"sync"
	"sync/atomic"
	"time"

	"github.com/cmu440/lspnet"
)

type client struct {
	// TODO: implement this!
	conn            *lspnet.UDPConn
	params          *Params
	connID          int
	seqNumGenerator int32
	lastReadSepNum  int32
	state           int32
	writeCond       *sync.Cond
	writeManager    *WriteManager
	readMsgs        *list.List
	readCond        *sync.Cond
	epochTimer      *time.Timer
	epoch           int
	heartbeat       *Heartbeat
	closeChan       chan bool
}

// NewClient creates, initiates, and returns a new client. This function
// should return after a connection with the server has been established
// (i.e., the client has received an Ack message from the server in response
// to its connection request), and should return a non-nil error if a
// connection could not be made (i.e., if after K epochs, the client still
// hasn't received an Ack message from the server in response to its K
// connection requests).
//
// hostport is a colon-separated string identifying the server's host address
// and port number (i.e., "localhost:9999").
func NewClient(hostport string, params *Params) (Client, error) {
	laddr, err := lspnet.ResolveUDPAddr("udp", "0.0.0.0:0")
	if err != nil {
		return nil, err
	}
	raddr, err := lspnet.ResolveUDPAddr("udp", hostport)
	if err != nil {
		return nil, err
	}
	conn, err := lspnet.DialUDP("udp", laddr, raddr)
	if err != nil {
		return nil, err
	}
	c := &client{
		conn:           conn,
		params:         params,
		connID:         -1,
		lastReadSepNum: 0,
		state:          StateIni,
		writeCond:      sync.NewCond(new(sync.Mutex)),
		readMsgs:       list.New(),
		readCond:       sync.NewCond(new(sync.Mutex)),
		writeManager:   NewWriteManager(),
		epochTimer:     time.NewTimer(0),
		heartbeat:      NewHeartbeat(),
		closeChan:      make(chan bool),
	}
	newConn := NewConnect()
	writeMsg(c.conn, nil, newConn)
	connChan := make(chan bool) // 连接等待chan

	go processLoop(c, connChan)
	for {
		select {
		case <-c.epochTimer.C:
			c.epoch++
			if c.epoch >= c.params.EpochLimit {
				return nil, errors.New("Error create connection timeout")
			}
			n, err := writeMsg(c.conn, nil, newConn)
			if n == 0 && err != nil {
				return nil, err
			}
			c.epochTimer.Reset(time.Duration(c.params.EpochMillis) * time.Millisecond)
		case cmd := <-connChan:
			if cmd {
				go processClientEpoch(c)
				go writeLoop(c)
				close(connChan)
				return c, nil
			}
		}
	}
}

func (c *client) ConnID() int {
	return c.connID
}

func (c *client) Read() ([]byte, error) {
	var readmsg *Message
	c.readCond.L.Lock()
	defer c.readCond.L.Unlock()
	for {
		if atomic.LoadInt32(&c.state) >= StateShutdown {
			return nil, errors.New("Client closed")
		}
		find := false
		for f := c.readMsgs.Front(); f != nil; {
			next := f.Next()
			pack := f.Value.(*ReadPack)
			readmsg = pack.Msg
			lsq := int(atomic.LoadInt32(&(c.lastReadSepNum)))
			if lsq+1 == readmsg.SeqNum {
				c.readMsgs.Remove(f)
				find = true
				break
			}
			f = next
		}
		if find {
			atomic.StoreInt32(&(c.lastReadSepNum), int32(readmsg.SeqNum))
			break
		}
		c.readCond.Wait()
	}
	return readmsg.Payload, nil
}

func (c *client) Write(payload []byte) error {
	state := atomic.LoadInt32(&c.state)
	if state >= StateShutdown {
		return errors.New("Client closed")
	}
	msg := NewData(c.ConnID(), NewSeqNum(&(c.seqNumGenerator)), len(payload), payload, 0)
	c.writeCond.L.Lock()
	defer c.writeCond.L.Unlock()
	c.writeManager.AddWaitPack(&WritePack{Msg: msg, startEpoch: -1})
	if state == StateRunning {
		c.writeCond.Signal()
	}
	return nil
}

func (c *client) Close() error {
	if atomic.LoadInt32(&c.state) >= StateShutdown {
		return errors.New("client already closed")
	}
	atomic.StoreInt32(&c.state, StateShutdown)
	if checkClientTerminated(c) {
		atomic.StoreInt32(&c.state, StateTerminated)
		c.conn.Close()
		notifyReadAndWrite(c.readCond, c.writeCond)
		return nil
	}
	for {
		select {
		case cmd := <-c.closeChan:
			if cmd {
				atomic.StoreInt32(&c.state, StateTerminated)
				c.conn.Close()
				notifyReadAndWrite(c.readCond, c.writeCond)
				return nil
			}
		}
	}

}

func processLoop(c *client, connChan chan bool) {
	buf := make([]byte, BUFFSIZE)
	for {
		if checkClientLoopStop(c) {
			break
		}
		msg, _, err := readMsg(c.conn, buf, false)
		if err != nil {
			cliLogger.Printf("Error occur when read message %s\n", err)
			break
		}
		c.heartbeat.setEpoch(c.epoch)
		if isDebugLogEnable() {
			cliLogger.Printf("Receive message %s\n", msg)
		}
		switch msg.Type {
		case MsgAck:
			if msg.SeqNum == 0 {
				if atomic.LoadInt32(&c.state) <= StateIni {
					atomic.StoreInt32(&c.state, StateRunning)
					c.connID = msg.ConnID
					connChan <- true
				}
			} else {
				c.writeCond.L.Lock()
				c.writeManager.HandleAck(msg.SeqNum)
				if c.writeManager.CheckWritable(c.params) {
					c.writeCond.Signal()
				}
				c.writeCond.L.Unlock()
				if checkClientTerminated(c) {
					c.closeChan <- true
					break
				}
			}
		case MsgData:
			if CalChecksum(msg.ConnID, msg.Payload) != msg.Checksum {
				cliLogger.Printf("Checksum incorrect %s", msg.String())
				//continue
			}
			if isDebugLogEnable() {
				cliLogger.Printf("Receive data %s\n", msg)
			}

			writeMsg(c.conn, nil, NewAck(msg.ConnID, msg.SeqNum))
			c.readCond.L.Lock()
			c.readMsgs.PushBack(&ReadPack{Msg: msg})
			c.readCond.Signal()
			c.readCond.L.Unlock()
		default:
			cliLogger.Printf("Unsupported message type %s", msg)
		}
	}
}

func writeLoop(c *client) {
	for {
		if checkClientLoopStop(c) {
			break
		}
		c.writeCond.L.Lock()
		for atomic.LoadInt32(&c.state) < StateRunning || !c.writeManager.CheckWritable(c.params) {
			c.writeCond.Wait()
		}
		if checkClientLoopStop(c) {
			c.writeCond.L.Unlock()
			break
		}
		f := c.writeManager.waitPacks.Front()
		msgPack := f.Value.(*WritePack)
		c.writeManager.waitPacks.Remove(f)
		msgPack.Msg.ConnID = c.ConnID()
		msgPack.Msg.Checksum = CalChecksum(c.ConnID(), msgPack.Msg.Payload)
		_, err := writeMsg(c.conn, nil, msgPack.Msg)
		c.writeManager.AddUnackPack(msgPack)
		c.writeCond.L.Unlock()
		c.heartbeat.addWriteCnt()

		if err != nil {
			servLogger.Printf("Write msg fail %s\n", err)
			break
		}
		if isDebugLogEnable() {
			servLogger.Printf("Write msg success %s\n", msgPack.Msg)
		}
	}
}

func processClientEpoch(c *client) {
	c.epochTimer.Reset(time.Duration(c.params.EpochMillis) * time.Millisecond)
	c.epoch = 0

	for {
		<-c.epochTimer.C
		if checkClientLoopStop(c) {
			break
		}
		// 一直没有读取到消息。 c.params.EpochLimit * c.params.EpochMillis
		if c.heartbeat.isTimeout(c.epoch, c.params.EpochLimit) {
			atomic.StoreInt32(&c.state, StateTerminated)
			epochTimeoutClose(c)
			if isDebugLogEnable() {
				cliLogger.Printf("Epoch close client %d\n", c.ConnID())
			}
			break
		}
		// 如果上一轮epoch没有发送任何消息，发heartbeat
		wm, _ := c.heartbeat.get()
		if wm == 0 {
			writeMsg(c.conn, nil, NewAck(c.connID, 0)) // heartbeat
		}
		c.heartbeat.setWriteCnt(0)

		c.writeCond.L.Lock()
		shouldClose := c.writeManager.CheckClosable(c.params, c.epoch)
		if !shouldClose {
			c.writeManager.TryRewrite(c.conn, nil, c.params, c.epoch)
			c.writeCond.Signal()
		}
		c.writeCond.L.Unlock()

		if shouldClose {
			atomic.StoreInt32(&c.state, StateTerminated)
			epochTimeoutClose(c)
			if isDebugLogEnable() {
				cliLogger.Printf("Epoch close client %d\n", c.ConnID())
			}
		}
		c.epoch++
		c.epochTimer.Reset(time.Duration(c.params.EpochMillis) * time.Millisecond)
	}
}

func checkClientTerminated(c *client) bool {
	writeLock := c.writeCond.L
	writeLock.Lock()
	defer writeLock.Unlock()
	b := c.writeManager.CheckWriteOver()
	return atomic.LoadInt32(&c.state) == StateShutdown && b
}

func checkClientLoopStop(c *client) bool {
	return atomic.LoadInt32(&c.state) >= StateTerminated
}

func epochTimeoutClose(c *client) {
	state := atomic.LoadInt32(&c.state)
	realClose := state != StateShutdown
	atomic.StoreInt32(&c.state, StateTerminated)
	if realClose {
		c.conn.Close()
		notifyReadAndWrite(c.readCond, c.writeCond)
	}
}
