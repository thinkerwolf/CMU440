package main

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/cmu440/bitcoin"
	"github.com/cmu440/lsp"
)

var (
	clientLogger = log.New(os.Stdout, "[bitcoin-client] ", log.LstdFlags)
)

func main() {
	const numArgs = 4
	if len(os.Args) != numArgs {
		fmt.Printf("Usage: ./%s <hostport> <message> <maxNonce>", os.Args[0])
		return
	}
	hostport := os.Args[1]
	message := os.Args[2]
	maxNonce, err := strconv.ParseUint(os.Args[3], 10, 64)
	if err != nil {
		fmt.Printf("%s is not a number.\n", os.Args[3])
		return
	}

	client, err := lsp.NewClient(hostport, lsp.NewParams())
	if err != nil {
		fmt.Println("Failed to connect to server:", err)
		return
	}

	defer client.Close()

	//_ = message  // Keep compiler happy. Please remove!
	//_ = maxNonce // Keep compiler happy. Please remove!
	// TODO: implement this!
	payload, _ := bitcoin.MarshalMessage(bitcoin.NewRequest(message, 0, maxNonce))
	err = client.Write(payload)
	if err != nil {
		clientLogger.Printf("Client %d write request err: %s\n", client.ConnID(), err)
		return
	}
	data, err := client.Read()
	m, err := bitcoin.UnmarshalMessage(data)
	if err != nil {
		clientLogger.Printf("Client %d unmarshal response err: %s\n", client.ConnID(), err)
		return
	}
	printResult(m.Hash, m.Nonce)
}

// printResult prints the final result to stdout.
func printResult(hash, nonce uint64) {
	fmt.Println("Result", hash, nonce)
}

// printDisconnected prints a disconnected message to stdout.
func printDisconnected() {
	fmt.Println("Disconnected")
}
