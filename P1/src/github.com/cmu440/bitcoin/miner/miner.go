package main

import (
	"fmt"
	"log"
	"os"

	"github.com/cmu440/bitcoin"
	"github.com/cmu440/lsp"
)

var (
	minerLogger = log.New(os.Stdout, "[bitcoin-miner] ", log.LstdFlags)
)

// Attempt to connect miner as a client to the server.
func joinWithServer(hostport string) (lsp.Client, error) {
	// TODO: implement this!
	return lsp.NewClient(hostport, lsp.NewParams())
}

func main() {
	const numArgs = 2
	if len(os.Args) != numArgs {
		fmt.Printf("Usage: ./%s <hostport>", os.Args[0])
		return
	}

	hostport := os.Args[1]
	miner, err := joinWithServer(hostport)
	if err != nil {
		fmt.Println("Failed to join with server:", err)
		return
	}

	defer miner.Close()

	// TODO: implement this!
	err = bitcoin.ClientWriteMessage(miner, bitcoin.NewJoin())
	if err != nil {
		return
	}

	hashCache := make(map[string]uint64)
	readChan := make(chan *bitcoin.Message)
	writeChan := make(chan *bitcoin.Message, 100)
	go minerReadLoop(miner, hashCache, readChan, writeChan)
	// go minerWriteLoop(miner, writeChan)

	for {
		payload, err := miner.Read()
		if err != nil {
			break
		}

		msg, err := bitcoin.UnmarshalMessage(payload)
		if err == nil {
			readChan <- msg
		}
	}
}

func minerReadLoop(c lsp.Client, hashCache map[string]uint64, readChan chan *bitcoin.Message, writeChan chan *bitcoin.Message) {
	for {
		msg := <-readChan
		cacheKey := fmt.Sprintf("%s_%d", msg.Data, msg.Nonce)
		hash, ok := hashCache[cacheKey]
		if !ok {
			hash = bitcoin.Hash(msg.Data, msg.Nonce)
			hashCache[cacheKey] = hash
		}
		result := bitcoin.NewResult(hash, msg.Nonce)
		result.Data = msg.Data
		result.Lower = msg.Lower
		result.Upper = msg.Upper
		result.Hash = hash
		err := bitcoin.ClientWriteMessage(c, result)
		if err != nil {
			fmt.Printf("Miner write error %s \n", err)
		}
		// fmt.Printf("Miner write resule %s %s\n", result, err)
	}
}
