package main

import (
	"container/list"
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/cmu440/bitcoin"
	"github.com/cmu440/lsp"
)

type server struct {
	lspServer      lsp.Server
	minerMap       map[int]*miner
	waitSendMsgs   *list.List
	readEventChan  chan *event
	errorEventChan chan *event
	dataNonceMap   map[string]*list.List
}

type miner struct {
	connID      int
	taskChan    chan *bitcoin.Message
	stopChan    chan interface{}
	unResultMap map[string]*bitcoin.Message
}

type event struct {
	connID  int
	payload []byte
	err     error
}

type request struct {
	connID     int
	msg        *bitcoin.Message
	receiveNum uint64
	minHash    uint64
	minNonce   uint64
}

func startServer(port int) (*server, error) {
	// TODO: implement this!
	lspServer, err := lsp.NewServer(port, lsp.NewParams())
	if err != nil {
		return nil, err
	}

	return &server{
		lspServer:      lspServer,
		minerMap:       make(map[int]*miner),
		waitSendMsgs:   list.New(),
		dataNonceMap:   make(map[string]*list.List),
		readEventChan:  make(chan *event),
		errorEventChan: make(chan *event),
	}, nil
}

var LOGF *log.Logger

func main() {
	// You may need a logger for debug purpose
	const (
		name = "log.txt"
		flag = os.O_RDWR | os.O_CREATE
		perm = os.FileMode(0666)
	)

	file, err := os.OpenFile(name, flag, perm)
	if err != nil {
		return
	}
	defer file.Close()

	LOGF = log.New(file, "", log.Lshortfile|log.Lmicroseconds)
	// Usage: LOGF.Println() or LOGF.Printf()

	const numArgs = 2
	if len(os.Args) != numArgs {
		fmt.Printf("Usage: ./%s <port>", os.Args[0])
		return
	}

	port, err := strconv.Atoi(os.Args[1])
	if err != nil {
		fmt.Println("Port must be a number:", err)
		return
	}

	srv, err := startServer(port)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	fmt.Println("Server listening on port", port)

	defer srv.lspServer.Close()

	// TODO: implement this!
	go serverLoop(srv)
	for {
		connID, payload, err := srv.lspServer.Read()
		srv.readEventChan <- &event{
			connID:  connID,
			payload: payload,
			err:     err,
		}
		if err != nil {
			fmt.Printf("Bitcoin server error %s \n", err)
		}
		if err != nil && connID == 0 {
			break
		}
	}
}

func getKey(connID int, msg *bitcoin.Message) string {
	format := "%d@%s_%d_%d"
	return fmt.Sprintf(format, connID, msg.Data, msg.Lower, msg.Upper)
}

func getDataNonceKey(data string, nonce uint64) string {
	return fmt.Sprintf("%s_%d", data, nonce)
}

func serverLoop(s *server) {
	for {
		select {
		case cmd := <-s.readEventChan:
			if cmd.err != nil {
				//TODO 故障处理
				if handleError(s, cmd) {
					break
				}
			} else {
				msg, err := bitcoin.UnmarshalMessage(cmd.payload)
				if err != nil {
					LOGF.Println("Server read unmarshal error:", err)
					continue
				}
				processServReadMsg(s, cmd.connID, msg)
			}
		case cmd := <-s.errorEventChan:
			// TODO  故障处理
			if handleError(s, cmd) {
				break
			}
		}

	}
}

func processServReadMsg(s *server, connID int, msg *bitcoin.Message) {
	switch msg.Type {
	case bitcoin.Join:
		// 加入矿机
		_, ok := s.minerMap[connID]
		if !ok {
			s.minerMap[connID] = &miner{
				connID:      connID,
				taskChan:    make(chan *bitcoin.Message),
				stopChan:    make(chan interface{}),
				unResultMap: make(map[string]*bitcoin.Message),
			}
			go minerLoop(s, s.minerMap[connID])
			for f := s.waitSendMsgs.Front(); f != nil; {
				scheduler(s, f.Value.(*bitcoin.Message))
				next := f.Next()
				s.waitSendMsgs.Remove(f)
				f = next
			}
		}
	case bitcoin.Request:
		// 客户端请求
		req := &request{
			connID: connID,
			msg:    msg,
		}
		isMinerEmpty := len(s.minerMap) == 0
		for n := msg.Lower; n <= msg.Upper; n++ {
			key := getDataNonceKey(msg.Data, n)
			waitList := s.dataNonceMap[key]
			if waitList == nil {
				waitList = list.New()
				s.dataNonceMap[key] = waitList
			}
			sc := waitList.Len() == 0
			waitList.PushBack(req)
			if sc {
				taskMsg := bitcoin.NewRequest(msg.Data, msg.Lower, msg.Upper)
				taskMsg.Nonce = n
				if isMinerEmpty {
					// 添加到等待列表
					s.waitSendMsgs.PushBack(taskMsg)
				} else {
					scheduler(s, taskMsg)
				}
			}
		}
	case bitcoin.Result:
		// TODO 矿机结果 finish
		m := s.minerMap[connID]
		key := getDataNonceKey(msg.Data, msg.Nonce)
		delete(m.unResultMap, key)
		waitList := s.dataNonceMap[key]
		for f := waitList.Front(); f != nil; {
			req := f.Value.(*request)
			req.receiveNum++
			if req.minHash == 0 || msg.Hash < req.minHash {
				req.minHash = msg.Hash
				req.minNonce = msg.Nonce
			}
			if req.receiveNum >= req.msg.Upper-req.msg.Lower+1 {
				result, _ := bitcoin.MarshalMessage(bitcoin.NewResult(req.minHash, req.minNonce))
				s.lspServer.Write(req.connID, result)
			}
			next := f.Next()
			waitList.Remove(f)
			f = next
		}

	}
}

func scheduler(s *server, taskMsg *bitcoin.Message) error {
	// choose one miner
	tn := -1
	var choose *miner
	for _, m := range s.minerMap {
		if tn < 0 || len(m.unResultMap) < tn {
			tn = len(m.unResultMap)
			choose = m
		}
	}
	choose.unResultMap[getDataNonceKey(taskMsg.Data, taskMsg.Nonce)] = taskMsg
	choose.taskChan <- taskMsg
	return nil
}

func minerLoop(s *server, miner *miner) {
	for {
		select {
		case msg := <-miner.taskChan:
			payload, err := bitcoin.MarshalMessage(msg)
			err = s.lspServer.Write(miner.connID, payload)
			if err != nil {
				s.errorEventChan <- &event{
					connID: miner.connID,
					err:    err,
				}
			}
		case <-miner.stopChan:
			break
		}
	}
}

func handleError(s *server, event *event) bool {
	if event.connID == 0 {
		for _, m := range s.minerMap {
			m.stopChan <- 1
		}
		return true
	}
	miner, ok := s.minerMap[event.connID]
	if ok {
		delete(s.minerMap, event.connID)
		miner.stopChan <- 1
		isMinerEmpty := len(s.minerMap) == 0
		for _, msg := range miner.unResultMap {
			if isMinerEmpty {
				// 添加到等待列表
				s.waitSendMsgs.PushBack(msg)
			} else {
				// 重新scheduler
				scheduler(s, msg)
			}
		}
	}
	return false
}
