package bitcoin

import (
	"encoding/json"

	"github.com/cmu440/lsp"
)

//UnmarshalMessage 消息反序列化
func UnmarshalMessage(payload []byte) (*Message, error) {
	msg := NewJoin()
	err := json.Unmarshal(payload, msg)
	if err != nil {
		return nil, err
	}
	return msg, nil
}

//MarshalMessage 消息序列化
func MarshalMessage(msg *Message) ([]byte, error) {
	data, err := json.Marshal(msg)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func ClientWriteMessage(cli lsp.Client, msg *Message) error {
	payload, err := MarshalMessage(msg)
	if err != nil {
		return err
	}
	err = cli.Write(payload)
	if err != nil {
		return err
	}
	return nil
}
